<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animal</title>
</head>
<body>
    
    <?php
    class Animal{
        public function __construct($name){
            $this->name = $name;
        }
        public $legs = 4;
        public $cold_blooded = "no";
    }


    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->name . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>   "; // "no"

    class Frog extends Animal{
        function jump(){
            echo ("Yell : " . "Hop Hop" . "<br><br>");
        }
    }
    class Ape extends Animal{
        function yell(){
            echo("Yell : " ."Auooo");
        }
        public $legs = 2;
    }

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>"; // "buduk"
    echo "Legs : " . $kodok->legs . "<br>"; // 4
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; // "no"
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->name . "<br>"; // "buduk"
    echo "Legs : " . $sungokong->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
    $sungokong->yell(); // "Auooo"
    ?>
</body>
</html>